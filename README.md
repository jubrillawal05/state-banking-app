# State Banking App 

A mock design of a banking application that offers users basic financial services (Deposit, Transfer, Transaction history)

### The Brief

**State Bank,** a commercial bank in Nigeria that has been in operation for a year, wishes to take their core services online. A platform that help their customers fast access to their services, improve their trust, bring in more potential customers and to better communicate the vision of the company to their customers.

To solve this, they want to create an e-platform accessible from mobile and web for utmost customer satisfaction.

### Project Goals

- To create delightful customers' experience
- Communicate the vision of the bank through this app.
- Simplify banking experiences for users.

### My Role

As the product designer for State Banking App, my tasks include:

- Design a webapp that allows users to deposit money and send it out to others, a visualization that shows deposit and withdrawal history and users' access to print out and share transactions.
- Design a simple flow that allows user to achieve their goals easily.
- Communicate the bank's commitment and vision of reliable services through the design of the app.

### My Approach

For this project, I will be developing the approach of the sprint model inspired by Jake Knapp's 'Design Sprint' book.

I am using this model because it is a trusted and practical way to design solutions coupled with time constraints. But few modifications, as I am not presented with the ideal sprint resources or environment. Also, to approach the solving of this problem uniquely.

![Brainwriting](/uploads/1d137b9804660d8597845894dcd7a6f2/Brainwriting.jpg)

### Day 1

I started off by speaking to the 'supposed' decision-maker for State Bank (my friend 'dave' played this role) on what the long term goal is. I have read the brief but what do they intend to achieve with the online platform. In his words " Earn our customers' trust by making our services easier to access."

**User Research**

I need to understand the potential/existing users, what kind of solution would meet their needs. I had to take a quick interview with them to get insights into their behaviour and mental model, so it could guide my decision making along the process.

I interviewed 5 people (both remotely and in-person) and had them take me through their experiences using a banking application. This helped me understand what an ideal banking app could be for users. The key message I got was;

Using a banking application isn't new to most users. They just want to be assured their details and money is protected and getting things done easily are what they care most about.

Secondly, from the brief it stated that users phone number represent the account number. I was quite curious on how users will feel; if it could make them fearful or not trust the application. But most of the people I interviewed feel it makes it easier for them to recall their account number and would want that.

Below is a mind map showing specific language and terms users used when they think of banking application. This is to better reflect the way the users think and talk in the actual interface.
![Brainwriting__1_](/uploads/1aa8e0b0d67b116bfef6382695f8c975/Brainwriting__1_.jpg)

**Competitive Analysis**

During the course of the interview, some users made references to the kuda app and standard chattered app. I started the market research from there, analysing the apps using the SWCDUXO model.  I analyzed the apps with reference to the user stories presented in the brief (not going beyond the scope of the brief). Below is the link to the document;
https://docs.google.com/spreadsheets/d/11YQDMrTuVAvnjU8ds7_sXYTBOE807Px2hDXPIGrkuJg/edit?usp=sharing

### Day 2

To meet our long term goal, what has to be true? (in the context of the user stories given in the brief). I came up with few sprint questions and a journey map.

**Sprint Questions ( Turning the problems into questions)**

- Can we make the sign up/ sign in seamless and fast?
- How can the depositing and sending out of money be straight to the point and easily understandable (even to those that are less tech savvy)

Below is a map visually showing the customers through the application.

![Brainwriting__2_](/uploads/4cdacf3eb38b35b3cb96d68870554606/Brainwriting__2_.jpg)

### Day 3

Once I decided the flow and the MVP, I was ready to start ideating and sketching up some solutions with the mind map I did earlier at the back of my mind. To create a solution that users need, drafted different solutions then picked the best out of it.
I started off with the mobile designs, the quick interview I had, showed users use their mobile for transactions more.

![Document_2](/uploads/e71f90e86c697143074e7a9ad4d93073/Document_2.jpg)
![Document_3](/uploads/4ada975a3208f3124620c3fa7264afae/Document_3.jpg)

### Day 4 & 5

**Visual Identity**
Coming up with some branding that reflects State Bank's originality. Using their brand colour in customising some of the icons and a logo that improves its customers' trust.

![Web_1920___1](/uploads/bca05a6d70158b04b457a3efd8c9df06/Web_1920___1.png)

**Prototype**
At this stage, I selected some of the sketches and converted them to high fidelity prototypes. I understood the interface has to make people feel secured and trust the platform. This influenced my choice of colour and the layout of the application (maintaining a minimalistic look and direct to the content).

Using the user stories provided, I will be explaining my decisions on designing the flow to help users achieve their goals.

![Purple_App_Phone_Mockup_Sales_Marketing_Presentation](/uploads/26ffc1d5e097adad519ac9dc58d01f13/Purple_App_Phone_Mockup_Sales_Marketing_Presentation.png)
![state_bank_ad](/uploads/4268f81ba20f4e64be42fa81bcb89783/state_bank_ad.mp4)

Please see a more detailed case study on notion
https://www.notion.so/State-Banking-App-64fcbdbe4da94991be5b7fc9a5353ea6

The Design Reource - https://drive.google.com/file/d/1Vwytt8xJdp7j3hNjIN8yh82tyvDec9dA/view?usp=sharing
